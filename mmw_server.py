#!/usr/bin/env python3
import common
import logging
log = logging.getLogger('server')
config = common.load_config()

import time
from pathlib import Path
import serial



class ComReader():
  CHUNK_SIZE = 1024

  def __init__(self, opts):
    self._opts = opts
    self.init()

  def read_data(self, n=CHUNK_SIZE):
    log.debug("attempting to read %d bytes from COM", n)
    data = self.com_data.read(n)
    log.debug("... actually read %d bytes (first 32: %s)", len(data), data[:32])
    return data

  def reinit(self):
    self.com_cli.close()
    self.com_data.close()
    time.sleep(0.1) # ??
    self.init()

  def init(self):
    self.com_cli = serial.Serial(*self._opts.client_port)
    self.com_data = serial.Serial(*self._opts.data_port, timeout=self._opts.data_timeout)

    for line in (line.strip() for line in Path(self._opts.setup_file).open('rb')):
      if line.startswith(b'%') or not line:
        log.debug('skipped config comment: %s', line)
        continue
      r = self.com_cli.write(line + b'\r\n')
      log.info('wrote config (%d): %s', r, line)
      time.sleep(0.02) # ??

    log.info("MMW reader initialized")


class MMWClientHandler(common.ClientHandler):
  def setup(self):
    super().setup()
    self._com = ComReader(self._opts)
    self._ok = True

  def handle(self):
    while self._ok:
      data = self._com.read_data()
      if not data:
        log.warning('sensor died? attempting to reinitialize...')
        self._com.reinit()
        continue
      log.info('sending %d bytes of data', len(data))
      self.request.sendall(data)

  def finish(self):
    super().finish()
    self._ok = False

if __name__ == '__main__':
  opts = config.mmw_server
  with common.SensorServer(opts, MMWClientHandler) as server:
    server.serve_forever()
