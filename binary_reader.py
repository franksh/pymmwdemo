
# More code extracted from personal libraries and sort of hacked at with a
# machete until it fit this project.

import logging
log = logging.getLogger('bin-reader')

import io
from collections import abc
import sys
from itertools import product
import struct

class StructuredIOError(RuntimeError):
  pass

class NotEnoughData(StructuredIOError):
  pass

class MatchFailed(StructuredIOError):
  pass


# MSB translation table for high->low bit reading of bytes where 0x80 is the
# first bit and 0x01 is the last bit.
MSB_ORDER = bytes(int(f'{k:08b}'[::-1], 2) for k in range(256))


class BinaryReader():
  """A generic bit/byte stream reader with backtracking and error-recovery
  capabilities.

  On a fundamental level it only relies on some function that returns bytes that
  can be recalled repeatedly. The stream might be finite or infinite, might be a
  file, socket, buffer, or anything.

  Currently the following types are supported out-of-the-box when given as an
  argument:

  - `str`: interpreted as a filename which is opened with `open(obj, 'rb')`.
  - `bytes`: taken as a fixed chunk of data to be read (wrapped in `BytesIO`).
  - `obj.read(n)` or `obj.recv(n)` exists: used in the customary way, as a
    file-like or socket I/O object.
  - `callable(obj)` is `True`: called with an integer argument (desired byte
    count) each time data is needed.
  - an iterable or generator: each time data is needed it calls `next(obj)`.

  The latter two should return an instance of `bytes`, `bytearray`, or `None`.
  `None` or an empty bytestring/-array is interpreted as end-of-stream.

  Bit reading:

  There is support for reading bytes in both bit orders. The default is `'lsb'`
  where 0x1 is the first bit and 0x80 is the last bit. Example: a "bit stream"
  `b'\x01\x80'` read in LSB bit order consists of a single 1-bit followed by
  fourteen 0-bits and then another 1-bit. In MSB order it would be seven 0-bits,
  two 1-bits, and another seven 0-bits. Mixing MSB and LSB reads within the same
  byte makes no sense and counts as a user error.

  The return value from bit reads they are always returned as an integer where
  bit `k` is `2^k`.

  Byte order:

  For regular byte-aligned reads byte order works as you would expect. For
  bit-aligned integer reads, the number is interpreted as if its
  most-significant bits are 0-bits to make it byte-aligned.

  Important differences between a `BinaryReader` and the usual Python file I/O
  objects:

  - An exception is raised when attempting to read more data than is available.
  - `with obj` starts a frame that is automatically committed or cancelled
    depending on whether an exception occurred.

  """

  # Bytes read when some unknown amount of data is needed.
  CHUNK_SIZE = 512

  def __init__(self, obj, start_offset=0, start_bit=0):
    self._stack = []
    self._retain = False
    self._offset = start_offset
    self._byteidx = 0
    self._bitidx = start_bit
    self._buf = bytearray()

    if isinstance(obj, str):
      obj = open(obj, 'rb')
    elif isinstance(obj, (bytes, bytearray)):
      obj = io.BytesIO(obj)

    if hasattr(obj, 'read'):
      self._read_func = obj.read
    elif hasattr(obj, 'recv'):
      self._read_func = obj.recv
    elif isinstance(obj, (abc.Iterable, abc.Generator)):
      it = iter(obj)
      def _read_iterator(_n):
        try:
          return next(it)
        except StopIteration:
          return None
      self._read_func = _read_iterator
    elif callable(obj):
      self._read_func = obj
    else:
      raise TypeError(f'not sure what to do about {type(obj)}')

  def try_feed(self, n=None):
    """Try to feed the buffer by reading from source.

    Reads some arbitrary amount of data. A hint can be given with the argument
    `n`, which defaults to `self.CHUNK_SIZE`.

    Returns `True` if more data was added, `False` otherwise.

    """
    data = self._read_func(n or self.CHUNK_SIZE)
    if data:
      self._buf.extend(data)
    return bool(data)

  def require(self, n):
    """Read data from source in a loop until the buffer contains at least `n` bytes.

    Raises `NotEnoughData` it if fails.

    """
    log.debug('requesting %d bytes (byteidx=%d, buflen=%d)', n, self._byteidx, len(self._buf))

    n += self._byteidx
    while (toread := n - len(self._buf)) > 0:
      if not self.try_feed(toread):
        raise NotEnoughData()

  def peek_bits(self, n, bitorder='lsb'):
    """Peeks at the next `n` bits in the stream.

    """
    if n == 0: return 0
    nb = (self._bitidx + n + 7) // 8
    byts = self.peek_bytes(nb)
    num = int.from_bytes(byts if bitorder == 'lsb' else byts.translate(MSB_ORDER), 'little')
    return (num >> self._bitidx) % (1 << n)

  def read_bits(self, n, bitorder='lsb'):
    """Reads the next `n` bits in the stream, increasing the current position.

    """
    res = self.peek_bits(n, bitorder)
    self._update_pos(*divmod(self._byteidx * 8 + self._bitidx + n, 8))
    return res

  def _update_pos(self, new_byteidx, new_bitidx):
    self._byteidx, self._bitidx = new_byteidx, new_bitidx
    if not self._retain:
      self._offset += self._byteidx
      self._buf = self._buf[self._byteidx:]
      self._byteidx = 0

  @property
  def byte_position(self):
    """Byte position in stream.

    Any bit offset within the current byte it is ignored.

    """
    return self._offset + self._byteidx

  @property
  def bit_position(self):
    """Bit position in stream.

    """
    return 8 * (self._offset + self._byteidx) + self._bitidx

  def peek_bytes(self, n):
    """Peeks at the next `n` bytes in the stream.

    WARNING: this method _ignores the bit offset within the current byte_ and
    only reads aligned bytes. If there is a bit offset and a possibly unaligned
    octet in an arbitrary bit stream is desired, use `peek_bits(8)` instead.

    """
    if n == 0: return b''
    self.require(n)
    return self._buf[self._byteidx:self._byteidx+n]

  def read_bytes(self, n):
    """Reads the next `n` bytes in the stream, increasing the current position.

    WARNING: this method _ignores the bit offset within the current byte_ and
    only reads aligned bytes. If there is a bit offset and a possibly unaligned
    octet in an arbitrary bit stream is desired, use `read_bits(8)` instead.

    """
    res = self.peek_bytes(n)
    self._update_pos(self._byteidx + n, 0)
    return res

  def frame_start(self):
    """Store the current byte and bit positions and possibly enter retain mode.

    This is intended to mark the start of a frame or structure, so that if
    reading the complete structure fails, the stream can be backtracked to a
    known valid position.

    `retain_mode` is activated if the underlying source does not support random
    access seeking. In `retain_mode`, the internal buffer is not drained, so all
    data read is retained. This makes backtracking possible in an unseekable
    stream.

    `frame_start()` works like a push onto a stack, so calling it multiple times
    will save multiple positions in a LIFO queue. Each position must be popped
    with either `frame_cancel()` or `frame_commit()`. Only when this stack is
    empty is `retain_mode` switched off if it was previously turned on.

    """
    self._stack.append((self._byteidx, self._bitidx))
    self._retain = True

  def frame_commit(self):
    """Signal that the current frame was read correctly and its start position can
    be forgotten.

    Note that in `retain_mode`, if there is still an active enclosing frame, the
    buffered data will still be retained.

    """
    self._stack.pop()
    if not self._stack:
      self._retain = False

  def frame_cancel(self):
    """Cancel the current frame, backtracking to the position whten the last
    `frame_start()` was called.

    """
    self._byteidx, self._bitidx = self._stack.pop()
    if not self._stack:
      self._retain = False

  @property
  def retain_mode(self):
    return self._retain

  def __enter__(self):
    self.frame_start()

  def __exit__(self, exc_type, *args):
    if exc_type is None:
      self.frame_commit()
    else:
      self.frame_cancel()

  def align(self, n=1):
    """Align position to an `n`-byte boundary.

    If `n <= 0` this is a no-op.

    If there is an active bit offset within the current byte, then `align(1)`
    can be used to align the stream to the next byte boundary.

    `n` defaults to 1.

    """
    if n <= 0:
      return
    extra = 1 if self._bitidx else 0
    return self.read_bytes(extra + -(self._byteidx + self._offset + extra) % n)

  def read_int(self, bits, signed=False, byteorder='native', bitorder='msb'):
    """Reads an arbitrary fixed-width integer from the binary stream.

    Arguments:

    - `bits`: bit width of the integer.
    - `signed`: whether to interpret the bits as a two's complement signed
      integer or not (default: `False`).
    - `byteorder`: `'little'`, `'big'`, or `'native'` (default: 'native').
    - `bitorder`: `'msb'` or `'lsb'`, same semantics as `peek_bits()`.

    Note that `read_int(k, byteorder='little')` is equivalent to `read_bits(k)`.

    Note that this is all done in pure Python and is thus relatively slow for a
    lot of repeated reads. If the bit sizes needed are standard (e.g. 8, 16, 32,
    or 64) you should encode your data structure as an `datastruct` instead.

    """
    n = self.read_bits(bits, bitorder)

    if byteorder == 'native':
      byteorder = sys.byteorder

    if byteorder != 'little':
      bs = n.to_bytes(bits // 8 + 1, 'big')
      n = int.from_bytes(bs[1:], 'little')
      n <<= (bits % 8)
      n += bs[0]

    if signed and n >> bits - 1:
      n -= 1 << bits

    return n

  def read_until(self, sentinel, skip=True, eof_ok=False):
    """Reads bytes until the byte sequence `sentinel` is encountered.

    - `skip`: `True` means the sentinel is consumed but not included in the
      return value, and the resulting stream position will be the next byte
      after the sentinel. `False` indicates the sentinel bytes should not be
      consumed, so the current stream position is where sentinel starts. The
      special value of `'include'` means the sentinel is read _and_ included in
      the returned value. (default: `True`)
    - `eof_ok` (bool): does end-of-stream count as encountering the sentinel?
      Note that the sentinel will not be included in this case even if
      `skip='include'`. (default: `False`)

    """
    if isinstance(sentinel, str):
      sentinel = sentinel.encode()

    while (i := self._buf.find(sentinel, self._byteidx)) == -1:
      if self.try_feed():
        continue
      if eof_ok and len(self._buf) > self._byteidx:
        return self.read_bytes(len(self._buf) - self._byteidx)
      raise NotEnoughData()

    if skip:
      i += len(sentinel)
    res = self.read_bytes(i - self._byteidx)
    return res[:-len(sentinel)] if skip == True else res

  def read_cstr(self, strip=True):
    """Reads bytes until a 0-byte is encountered.

    A synonym for `read_until(b'\x00', skip=strip)`.

    """
    return self.read_until(b'\x00', consume=strip)

  def read_line(self, strip=True):
    """Reads bytes until a newline is encountered.

    A synonym for `read_until(b'\n', skip=strip, eof_ok=True)`.

    """
    return self.read_until(b'\n', strip, eof_ok=True)

  def read_regex(self, pat):
    """Reads data until the given regex matches at the current position in the stream.

    Raises `FailedMatch` if the regex cannot match.

    WARNING: be wary of what kind of patterns used. Something like `rb'.*a'`
    might cause the stream to be read infinitely (or until empty) if the `b'a'`
    is never found.

    """
    assert isinstance(pat, (regex.Pattern, str, bytes))

    if isinstance(pat, str):
      pat = pat.encode()
    if isinstance(pat, bytes):
      pat = regex.compile(pat)

    while m := pat.match(self._buf, partial=True):
      if not m.partial:
        return m
      if not self.try_feed():
        break

    raise FailedMatch()

  def read_struct(self, desc):
    if isinstance(desc, str):
      desc = struct.Struct(desc)

    if isinstance(desc, struct.Struct):
      return desc.unpack(self.read_bytes(desc.size))

    raise TypeError(f'not sure what to do with {type(desc)}')
