
An unfinished, hacky AWR1843 mmwave demo thing written in Python.

## Requirements

``` sh
pip install pyqtgraph regex numpy pyserial click
```

Probably Python 3.8+. Maybe 3.9 just to be safe.

Maybe also `pip install pyqt5` to make it use the same PyQt as I do?

## Config

See `config_ex.cfg`. I've included some comments on what the various commands
mean since its documentation is kind of scattered in the SDK source.

## Run

The main script is `mmw_dummy.py`:

``` sh
LOGLEVEL=debug python mmw_dummy.py config_ex.cfg
```

## Screenshot

![screenshot](screenshot.png)
