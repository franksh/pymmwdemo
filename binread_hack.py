#!/usr/bin/env python3

import common
import logging
log = logging.getLogger('binread-hack')

from collections import namedtuple

# This is more like a hacky experiment than anything else, just to see if it
# would work.
#
# It's an answer to a question no one should be asking.
#
# The packet parser yields requests to a handler which does the actual
# interaction with the reader and catches NotEnoughData exceptions.
#
# And yeah the use of the okina character in the names are there to emphasize
# how hacky it is.

poor_manʻs_future = namedtuple('poor_manʻs_future', 'meth args kwargs')

class poor_manʻs_shill():
  __slots__ = ('_r', )
  def __init__(self, r):
    self._r = r
  def __getattr__(self, key):
    m = getattr(self._r, key)
    if not callable(m):
      return m
    return lambda *args, **kwargs: poor_manʻs_future(m, args, kwargs)

def poor_manʻs_coroutines(r, func):
  r = poor_manʻs_shill(r)
  gen = func(r)
  val = None
  while True:
    try:
      what = gen.send(val)
    except StopIteration as si:
      yield si.args[0]
      gen = func(r)
      val = None
      continue
    except ValueError:
      yield ()

    while True:
      try:
        val = what.meth(*what.args, **what.kwargs)
      except NotEnoughData:
        log.info("not enough data")
        yield None
      else:
        break
