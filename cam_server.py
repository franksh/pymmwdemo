#!/usr/bin/env python3
import common
import logging
log = logging.getLogger('camera')
config = common.load_config()

import picamera
import numpy as np

class CamReader():
  def __init__(self, opts):
    self._opts = opts
    self.init()

  def read_data(self):
    log.debug('capturing image (format=%s, size=%s)', self._opts.format, self._opts.output_resolution)
    _ = next(self._data_gen)
    return self._data.tobytes()

  def reinit(self):
    self._cam.close()
    time.sleep(0.1) # ??
    self.init()

  def init(self):
    self._cam = picamera.PiCamera()
    self._cam.resolution = opts.capture_resolution
    self._cam.framerate = opts.framerate
    self._data = np.zeros(shape=(opts.output_resolution[1], opts.output_resolution[0], 3), dtype=np.uint8)
    self._data_gen = self._cam.capture_continuous(self._data,
                                                  format=self._opts.format,
                                                  resize=self._opts.output_resolution)
    log.info("camera reader initialized")


class CamClientHandler(common.ClientHandler):
  def setup(self):
    super().setup()
    self._cam = CamReader(self._opts)

  def handle(self):
    while True:
      data = self._cam.read_data()
      log.info('sending %d bytes of data', len(data))
      self.request.sendall(b'<cam$sync>')
      self.request.sendall(len(data).to_bytes(4, 'big'))
      self.request.sendall(data)


if __name__ == '__main__':
  opts = config.cam_server
  with common.SensorServer(opts, CamClientHandler) as server:
    server.serve_forever()
