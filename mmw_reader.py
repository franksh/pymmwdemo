#!/usr/bin/env python3
import common
import logging
log = logging.getLogger('mmw')
config = common.load_config()

import time
from pathlib import Path
import struct
from collections import namedtuple
from enum import Enum, auto
from dataclasses import dataclass, field
import binread_hack
from binary_reader import BinaryReader, NotEnoughData
import numpy as np


class MMW_TYPE(Enum):
  "MMW TLV tag types."
  DETECTED_POINTS = 1
  RANGE_PROFILE = 2
  NOISE_PROFILE = 3
  AZIMUT_STATIC_HEAT_MAP = 4
  RANGE_DOPPLER_HEAT_MAP = 5
  STATS = 6
  DETECTED_POINTS_SIDE_INFO = 7
  AZIMUT_ELEVATION_STATIC_HEAT_MAP = 8
  TEMPERATURE_STATS = 9


@dataclass
class mmw_packet_t:
  magic: bytes
  version: int
  total_len: int
  platform: int
  frame_idx: int
  cpu_cycle: int
  num_objs: int
  num_tlvs: int
  subframe_idx: int
  DATA: object = field(default_factory=list)
  padding: bytes = b''

  def tlv(self, idx):
    for t in self.DATA:
      if t[0] == idx:
        return t[2]

  def __repr__(self):
    return f"""
PACKET(idx=({self.frame_idx},{self.subframe_idx}),time={self.cpu_cycle},
       plat={self.platform:x},vers={self.version:x},
       objs={self.num_objs},tlvs={self.num_tlvs}, LEN={self.total_len})""".strip()

  def np_objs(self):
    return np.array([
      [pt.x, pt.y, pt.z, pt.velocity, si.snr/10.0, si.noise/10.0]
      for pt,si in zip(self.tlv(MMW_TYPE.DETECTED_POINTS),
                       self.tlv(MMW_TYPE.DETECTED_POINTS_SIDE_INFO))])

  def np_range_profile(self):
    return np.frombuffer(self.tlv(MMW_TYPE.RANGE_PROFILE), dtype=np.uint16)

  def np_doppler_heatmap(self):
    return np.frombuffer(self.tlv(MMW_TYPE.RANGE_DOPPLER_HEAT_MAP), dtype=np.int16)

tlv_packet = namedtuple('tlv_packet', 'tag length DATA', defaults=(None,))

pc_side_info = namedtuple('pc_side_info', 'snr noise')
pc_cartesian = namedtuple('pc_cartesian', 'x y z velocity')


# class MMWReader():
#   def __init__(self, setup_file, client_port, data_port, data_timeout=0.5):
#     self.com_cli = serial.Serial(*client_port)
#     self.com_data = serial.Serial(*data_port, timeout=data_timeout)

#     for line in (line.strip() for line in Path(setup_file).open('rb')):
#       if line.startswith(b'%') or not line:
#         log.debug('skipped config comment: %s', line)
#         continue
#       self.com_cli.write(line + b'\n')
#       log.info('wrote config: %s', line)
#       time.sleep(0.02) # ??

#     self._reader = BinaryReader(self.read_com)
#     self._stream = binread_hack.poor_manʻs_coroutines(self._reader, ʻʻasyncʻʻ_mmw_demo_packet)

#   def read_com(self, n):
#     n = max(n, 32)
#     log.debug("attempting to read %d bytes from COM", n)

#     data = self.com_data.read(n)
#     log.debug("... actually read %d bytes (first 32: %s)", len(data), data[:32])
#     return data

#   def get_packet(self):
#     data = next(self._stream)
#     if data == ():
#       self.log.error("error occurred in reading, resetting stream")
#       self._stream = poor_manʻs_coroutines(self._reader, ʻʻasyncʻʻ_mmw_demo_packet)
#       return None

#     return data


class MMWReader(BinaryReader):
  def sync_to_packet(self):
    # Sync to magic.
    predata = self.read_until(bytes.fromhex('0201040306050807'), skip=False)
    if predata:
      log.warning('skipped unknown %d bytes of data: %s', len(predata), predata[:256])
    return True

  def read_data(self):
    assert self.sync_to_packet()

    try:
      with self:
        return self.read_packet()
    except (ValueError, AssertionError):
      log.exception("invalid packet, skipping sync token")
      self.read_bytes(8)
      raise NotEnoughData

  def read_packet(self):
    # Header.
    start_pos = self.byte_position
    head = mmw_packet_t(*self.read_struct('=8s8I'))

    log.debug("HEADER: %s", repr(head))

    # Subpackets.
    for _ in range(head.num_tlvs):
      tag, tlv_len = self.read_struct('=2I')
      tag = MMW_TYPE(tag)
      _p = self.byte_position
      data = []
      log.debug("TLV: %s, %d", tag, tlv_len)

      if tag == MMW_TYPE.DETECTED_POINTS:
        for _ in range(head.num_objs):
          data.append(pc_cartesian(*self.read_struct('4f')))
      elif tag == MMW_TYPE.DETECTED_POINTS_SIDE_INFO:
        for _ in range(head.num_objs):
          data.append(pc_side_info(*self.read_struct('2h')))
      elif tag == MMW_TYPE.RANGE_PROFILE:
        data = self.read_bytes(tlv_len)
      elif tag == MMW_TYPE.RANGE_DOPPLER_HEAT_MAP:
        data = self.read_bytes(tlv_len)
      elif tag == MMW_TYPE.NOISE_PROFILE:
        data = self.read_bytes(tlv_len)
      else:
        assert False, f"unknown tag {tag}"

      assert self.byte_position - _p == tlv_len
      head.DATA.append((tag, tlv_len, data))

    cur_len = self.byte_position - start_pos
    assert cur_len <= head.total_len
    head.padding = self.read_bytes(head.total_len - cur_len)
    return head


# Avert thine eyes, childe. See comment in binread_hack.py.
def ʻʻasyncʻʻ_mmw_demo_packet(r):
  # Sync to magic.
  predata = yield r.read_until(bytes.fromhex('0201040306050807'), skip=False)
  if predata:
    log.warning('skipped unknown %d bytes of data: %s', len(predata), predata)

  # Header.
  start_pos = r.byte_position
  head = mmw_packet_t(*(yield r.read_struct('=8s8I')))

  log.info("HEADER: %s", repr(head))

  # Subpackets.
  for _ in range(head.num_tlvs):
    tag, tlv_len = yield r.read_struct('=2I')
    tag = MMW_TYPE(tag)
    _p = r.byte_position
    data = []
    log.info("TLV: %s, %d", tag, tlv_len)

    if tag == MMW_TYPE.DETECTED_POINTS:
      for _ in range(head.num_objs):
        data.append(pc_cartesian(*(yield r.read_struct('4f'))))
    elif tag == MMW_TYPE.DETECTED_POINTS_SIDE_INFO:
      for _ in range(head.num_objs):
        data.append(pc_side_info(*(yield r.read_struct('2h'))))
    elif tag == MMW_TYPE.RANGE_PROFILE:
      data = yield r.read_bytes(tlv_len)
    elif tag == MMW_TYPE.RANGE_DOPPLER_HEAT_MAP:
      data = yield r.read_bytes(tlv_len)
    elif tag == MMW_TYPE.NOISE_PROFILE:
      data = yield r.read_bytes(tlv_len)
    else:
      assert False, f"unknown tag {tag}"

    assert r.byte_position - _p == tlv_len
    head.DATA.append((tag, tlv_len, data))

  cur_len = r.byte_position - start_pos
  assert cur_len <= head.total_len
  head.padding = yield r.read_bytes(head.total_len - cur_len)
  return head
