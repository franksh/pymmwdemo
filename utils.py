# Stuff extracted from my personal libraries.

import numpy as np
import operator as op

def try_int(n, default=None):
  """Tries to interpret `n` as an `int`.

  If the number can't be type-converted to a Python `int` (i.e. it lacks
  `__index__()`), then `default` will be returned instead (default: `None`).

  This is useful for handling "foreign" integers such as from `gmpy2` or `numpy`
  which do not subclass `int` so the usual test of `isinstance(x, int)` does not
  work.

  """
  try:
    return op.index(n)
  except:
    return default

class moving_average():
  """History tracking of a value.

  If initialized with a number this is taken as the length of the window. The
  array is zero-initialized.

  >>> m = value_history(5>>> m = value_history(5)
  >>> m(1)
  0.2
  >>> m(1)
  0.4
  >>> m
  array([0., 0., 0., 1., 1.])

  If given something convertible to a list, this is taken as the previous values
  and.

  >>> m = value_history([0.0,20.0,30.0])
  >>> m()
  16.666666666666668
  >>> m(10.0)
  20.0

  - `obj()`: returns the mean (moving average) of the values.
  - `obj(x)`: adds the value `x` (and returns the mean).
  - `obj.height`: the difference between the largest and smallest value in the
    history.

  Internally represented by a wrapping index into a NumPy array.

  """
  def __init__(self, hist):
    if n := try_int(hist):
      self._arr = np.zeros((n,), np.float64)
    else:
      self._arr = np.array(list(hist))

    self._idx = 0
    self._mod = len(self._arr)
    if self._mod < 1:
      raise ValueError(f"length must be >= 1")

  def __repr__(self):
    return repr(self.V)

  @property
  def V(self):
    """Copy of underlying array rolled so the last value is `V[-1]`.

    """
    return np.roll(self._arr, -self._idx)

  @property
  def height(self):
    return self._arr.max() - self._arr.min()

  def __getattr__(self, key):
    return getattr(self.V, key)

  def __getitem__(self, idx):
    return self.V[idx]

  def __call__(self, value=None):
    if value is not None:
      self._arr[self._idx % self._mod] = value
      self._idx += 1
    return self._arr.mean()
