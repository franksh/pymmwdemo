#!/usr/bin/env python3
import common
import logging
log = logging.getLogger('client')
config = common.load_config()

import socket
from pathlib import Path
import click
import sys

@click.command()
@click.argument('host')
@click.argument('port', type=int, default=config.server.port)
def main(host, port):
  s = socket.create_connection((host, port))
  #socket.socket(socket.AF_INET, socket.SOCK_STREAM | ..)socket.print(host, port)

  while True:
    data = s.recv(2000)
    print(len(data), data[:20])

if __name__ == '__main__':
  main()
