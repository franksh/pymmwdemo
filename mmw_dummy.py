import os
import logging
logging.basicConfig(
  level=os.environ.get('LOGLEVEL', 'INFO').upper())
log = logging.getLogger('main')

import sys
import serial
import time
from pathlib import Path
import numpy as np
np.set_printoptions(linewidth=300)

from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
import pyqtgraph as pg

# Avert thine eyes, childe.
from mmw_data import (BinaryReader,
                      poor_manʻs_coroutines,
                      ʻʻasyncʻʻ_mmw_demo_packet)

class MMWave():
  def __init__(self, config_file, cli_port='/dev/ttyACM0', data_port='/dev/ttyACM1'):
    self.com_cli = serial.Serial(cli_port, 115200)
    self.com_data = serial.Serial(data_port, 921600, timeout=0.5)
    self.log = log.getChild('mmwave')

    for line in (line.strip() for line in Path(config_file).open('rt')):
      if line.startswith('%') or not line:
        self.log.debug('skipped config comment: %s', line)
        continue
      self.com_cli.write(line.encode() + b'\n')
      self.log.info('wrote config: %s', line)
      time.sleep(0.02) # ??

    self._reader = BinaryReader(self.read_com)
    self._stream = poor_manʻs_coroutines(self._reader, ʻʻasyncʻʻ_mmw_demo_packet)

  def read_com(self, n):
    n = max(n, 32)
    log.debug("attempting to read %d from COM", n)
    data = self.com_data.read(n)
    log.debug("... actually read %d", len(data))
    return data

  def get_packet(self):
    data = next(self._stream)
    if data == ():
      self.log.warning("resetting stream")
      self._stream = poor_manʻs_coroutines(self._reader, ʻʻasyncʻʻ_mmw_demo_packet)
      return None

    if data is None:
      return None

    print(data)
    return data


class MMWPlotter(QtWidgets.QSplitter):
  def __init__(self, view, mmw, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.log = log.getChild('plot')
    self._view = view
    self._mmw = mmw
    plot1 = view.addPlot(title='detected objects')
    plot1.showGrid(x=True, y=True)
    plot1.setLabel('left', text ='Y position (m)')
    plot1.setLabel('bottom', text='X position (m)')
    self._scatter1 = pg.ScatterPlotItem()
    plot1.addItem(self._scatter1, row=0, col=0)

    plot2 = view.addPlot(title='ranges profile', row=1, col=0)
    self._line1 = plot2.plot()

    self._cm = pg.colormap.get('CET-R4')
    self._first = True

    self.addWidget(view)

    self._img = pg.ImageView()
    self._img.setPredefinedGradient('flame')
    self._img.setLevels(1000, 10000)
    self._img.setHistogramRange(1000, 10000)
    self._img.view.setAspectLocked(False)
    self.addWidget(self._img)
    self.show()

  def update(self):
    try:
      packet = self._mmw.get_packet()
    except StopIteration:
      self.log.error("some error occurred, probably the COM died, good luck")
      self.close()
      return
    if packet is None:
      return

    objs = packet.np_objs().T
    doppler = packet.np_doppler_heatmap()
    rang = packet.np_range_profile()

    doppler = doppler.reshape(256, -1).T
    doppler = np.roll(doppler, 8, 0)
    self.log.debug(f"arrays: {objs.shape=}, {doppler.shape=}, {rang.shape=}")
    # for i in range(6):
    #   dr[i](arr[i].max())
    #   dr[i](arr[i].min())
    #   print(f'{i} {dr[i].min} {dr[i].max}')

    self._scatter1.setData(
      x=objs[0], y=objs[1],
      size=10,
      symbol='o',
      pen='black',
      brush=[self._cm.mapToQColor(x+0.5) for x in objs[3]]
    )
    self._line1.setData(rang)
    self._img.setImage(
      doppler,
      autoRange=False, # self._first,
      autoHistogramRange=False, # self._first,
      autoLevels=False, # self._first,
    )
    self._first = False

  def closeEvent(self, evt):
    self.log.warning("close event")
    timer.stop()
    super().closeEvent(evt)


# Hack.
log_file = None
timer = None
mmw = None

import click

@click.command()
@click.option('-l', '--doppler-file', help='File to dump Range Doppler data to.')
@click.option('-c', '--config', default='config_ex.cfg', show_default=True, help='Config file.')
def main(config, doppler_file):
  """MMW Visualizer."""
  global log_file, timer, mmw

  mmw = MMWave(config)

  app = pg.mkQApp('app name')
  pg.setConfigOptions(antialias=True)
  view = pg.GraphicsLayoutWidget(title='widget title')

  mmwp = MMWPlotter(view, mmw)
  mmwp.setWindowTitle('window title')

  if doppler_file:
    log_file = open(doppler_file, 'wb')
  timer = QtCore.QTimer()
  timer.timeout.connect(mmwp.update)
  timer.start(20)
  pg.exec()

if __name__ == '__main__':
  main()
