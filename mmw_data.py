
# All of this shit is just pure hacky hacks.

import logging
log = logging.getLogger('mmw_data')

import traceback
import math # XXX

from functools import wraps
from binary_reader import *
import struct
from collections import namedtuple
from enum import Enum, auto
from dataclasses import dataclass, field

import numpy as np


class MMW_TYPE(Enum):
  "MMW TLV tag types."
  DETECTED_POINTS = 1
  RANGE_PROFILE = 2
  NOISE_PROFILE = 3
  AZIMUT_STATIC_HEAT_MAP = 4
  RANGE_DOPPLER_HEAT_MAP = 5
  STATS = 6
  DETECTED_POINTS_SIDE_INFO = 7
  AZIMUT_ELEVATION_STATIC_HEAT_MAP = 8
  TEMPERATURE_STATS = 9


@dataclass
class mmw_packet_t:
  magic: bytes
  version: int
  total_len: int
  platform: int
  frame_idx: int
  cpu_cycle: int
  num_objs: int
  num_tlvs: int
  subframe_idx: int
  DATA: object = field(default_factory=list)
  padding: bytes = b''

  def tlv(self, idx):
    for t in self.DATA:
      if t[0] == idx:
        return t[2]

  def __repr__(self):
    return f"""
PACKET(idx=({self.frame_idx},{self.subframe_idx}),time={self.cpu_cycle},
       plat={self.platform:x},vers={self.version:x},
       objs={self.num_objs},tlvs={self.num_tlvs}, LEN={self.total_len})""".strip()

  def np_objs(self):
    return np.array([
      [pt.x, pt.y, pt.z, pt.velocity, si.snr/10.0, si.noise/10.0]
      for pt,si in zip(self.tlv(MMW_TYPE.DETECTED_POINTS),
                       self.tlv(MMW_TYPE.DETECTED_POINTS_SIDE_INFO))])

  def np_range_profile(self):
    return np.frombuffer(self.tlv(MMW_TYPE.RANGE_PROFILE), dtype=np.uint16)

  def np_doppler_heatmap(self):
    return np.frombuffer(self.tlv(MMW_TYPE.RANGE_DOPPLER_HEAT_MAP), dtype=np.int16)

tlv_packet = namedtuple('tlv_packet', 'tag length DATA', defaults=(None,))

pc_side_info = namedtuple('pc_side_info', 'snr noise')
pc_cartesian = namedtuple('pc_cartesian', 'x y z velocity')


# This is more like a hacky experiment than anything else, just to see if it
# would work.
#
# It's an answer to a question no one should be asking.
#
# The packet parser yields requests to a handler which does the actual
# interaction with the reader and catches NotEnoughData exceptions.
#
# And yeah the names are there to emphasize how hacky it is.
poor_manʻs_future = namedtuple('poor_manʻs_future', 'meth args kwargs')
class poor_manʻs_shill():
  __slots__ = ('_r', )
  def __init__(self, r):
    self._r = r
  def __getattr__(self, key):
    m = getattr(self._r, key)
    if not callable(m):
      return m
    return lambda *args, **kwargs: poor_manʻs_future(m, args, kwargs)

def poor_manʻs_coroutines(r, func):
  r = poor_manʻs_shill(r)
  gen = func(r)
  val = None
  while True:
    try:
      what = gen.send(val)
    except StopIteration as si:
      yield si.args[0]
      gen = func(r)
      val = None
      continue
    except ValueError:
      yield ()

    while True:
      try:
        val = what.meth(*what.args, **what.kwargs)
      except NotEnoughData:
        log.info("not enough data")
        yield None
      else:
        break

def ʻʻasyncʻʻ_mmw_demo_packet(r):
  # Sync to magic.
  predata = yield r.read_until(bytes.fromhex('0201040306050807'), skip=False)
  if predata:
    log.warning(f'skipped unknown data: {predata}')

  start_pos = r.byte_position
  head = mmw_packet_t(*(yield r.read_struct('=8s8I')))

  log.info("HEADER: %s", repr(head))

  for _ in range(head.num_tlvs):
    tag, tlv_len = yield r.read_struct('=2I')
    tag = MMW_TYPE(tag)
    _p = r.byte_position
    data = []
    log.info("TLV: %s, %d", tag, tlv_len)

    if tag == MMW_TYPE.DETECTED_POINTS:
      for _ in range(head.num_objs):
        data.append(pc_cartesian(*(yield r.read_struct('4f'))))
    elif tag == MMW_TYPE.DETECTED_POINTS_SIDE_INFO:
      for _ in range(head.num_objs):
        data.append(pc_side_info(*(yield r.read_struct('2h'))))
    elif tag == MMW_TYPE.RANGE_PROFILE:
      data = yield r.read_bytes(tlv_len)
    elif tag == MMW_TYPE.RANGE_DOPPLER_HEAT_MAP:
      data = yield r.read_bytes(tlv_len)
    elif tag == MMW_TYPE.NOISE_PROFILE:
      data = yield r.read_bytes(tlv_len)
    else:
      assert False, f"unknown tag {tag}"

    assert r.byte_position - _p == tlv_len
    head.DATA.append((tag, tlv_len, data))

  cur_len = r.byte_position - start_pos
  assert cur_len <= head.total_len
  head.padding = yield r.read_bytes(head.total_len - cur_len)
  return head


