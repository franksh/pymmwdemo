#!/usr/bin/env python3
import common
import logging
log = logging.getLogger('client')
config = common.load_config()

from pyqtgraph.Qt import QtGui, QtCore, QtWidgets
import pyqtgraph as pg
import numpy as np

import socket
from pathlib import Path
import click
import sys

import binary_reader as br
import mmw_reader


class ReadThread(QtCore.QThread):
  new_data = QtCore.pyqtSignal(object)
  socket_closed = QtCore.pyqtSignal()

  def __init__(self, socket, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self._socket = socket
    self._reader = self.mk_reader()
    self._ok = True

  def mk_reader(self):
    """Abstract method: override this.

    """
    raise NotImplementedError()

  def read_socket(self, n):
    data = self._socket.recv(n)
    if not data:
      self._ok = False
      self.socket_closed.emit()
    return data

  def run(self):
    while self._ok:
      try:
        packet = self._reader.read_data()
      except br.NotEnoughData:
        self.yieldCurrentThread()
      else:
        log.info('emitting new packet from %s', type(self).__name__)
        self.new_data.emit(packet)

class MMWThread(ReadThread):
  def mk_reader(self):
    return mmw_reader.MMWReader(self.read_socket)


class CamReader(br.BinaryReader):
  def sync_to_packet(self):
    predata = self.read_until(b'<cam$sync>', skip=False)
    if predata:
      log.warning('skipped unknown %d bytes of data: %s', len(predata), predata)
    return True

  def read_data(self):
    assert self.sync_to_packet()

    with self:
      assert self.read_bytes(10) == b'<cam$sync>'

      n = self.read_int(32, byteorder='big', bitorder='lsb')
      assert 1_000 < n < 1_000_000

      imgdata = self.read_bytes(n)

    img = np.frombuffer(imgdata, dtype=np.uint8)
    log.info("read image of size %d", len(imgdata))
    return img


class CamThread(ReadThread):
  def mk_reader(self):
    return CamReader(self.read_socket)


class MMWPlotter(QtWidgets.QSplitter):
  def __init__(self, view, *args, **kwargs):
    super().__init__(*args, **kwargs)

    self.log = log.getChild('plot')
    self._view = view
    plot1 = view.addPlot(title='detected objects')
    plot1.showGrid(x=True, y=True)
    plot1.setLabel('left', text ='Y position (m)')
    plot1.setLabel('bottom', text='X position (m)')
    self._scatter1 = pg.ScatterPlotItem()
    plot1.addItem(self._scatter1, row=0, col=0)

    plot2 = view.addPlot(title='ranges profile', row=1, col=0)
    self._line1 = plot2.plot()

    self._cm = pg.colormap.get('CET-R4')
    self._first = True

    self.addWidget(view)

    self._img = pg.ImageView()
    self._img.setPredefinedGradient('flame')
    self._img.setLevels(1000, 10000)
    self._img.setHistogramRange(1000, 10000)
    self._img.view.setAspectLocked(False)
    self.addWidget(self._img)

    self._cam = pg.ImageView()
    self._cam.view.setAspectLocked(False)
    self.addWidget(self._cam)
    self.show()

  def update_cam(self, packet):
    w,h = config.cam_server.output_resolution
    packet = packet.reshape(h, w, 3)
    self._cam.setImage(packet,
      autoRange=False, # self._first,
      autoHistogramRange=False, # self._first,
      autoLevels=False, # self._first,
                       )

  def update_mmw(self, packet):
    objs = packet.np_objs().T
    doppler = packet.np_doppler_heatmap()
    rang = packet.np_range_profile()

    doppler = doppler.reshape(256, -1).T
    doppler = np.roll(doppler, 8, 0)
    self.log.debug(f"arrays: {objs.shape=}, {doppler.shape=}, {rang.shape=}")
    # for i in range(6):
    #   dr[i](arr[i].max())
    #   dr[i](arr[i].min())
    #   print(f'{i} {dr[i].min} {dr[i].max}')

    self._scatter1.setData(
      x=objs[0], y=objs[1],
      size=10,
      symbol='o',
      pen='#000000',
      brush=[self._cm.mapToQColor(x+0.5) for x in objs[3]]
    )
    self._line1.setData(rang)
    self._img.setImage(
      doppler,
      autoRange=False, # self._first,
      autoHistogramRange=False, # self._first,
      autoLevels=False, # self._first,
    )
    self._first = False

  def closeEvent(self, evt):
    self.log.warning("close event")
    super().closeEvent(evt)


@click.command()
@click.option('-l', '--doppler-file', help='File to dump Range Doppler data to.')
@click.argument('host')
def main(doppler_file, host):
  """MMW Visualizer."""
  # if doppler_file:
  #   log_file = open(doppler_file, 'wb')
  app = pg.mkQApp('pysensor mmw')

  pg.setConfigOptions(antialias=True)
  view = pg.GraphicsLayoutWidget(title='widget title')

  mmwp = MMWPlotter(view)
  mmwp.setWindowTitle('window title')

  try:
    mmw_sock = socket.create_connection((host, config.mmw_server.port))
    mmw_thread = MMWThread(mmw_sock)
    mmw_thread.new_data.connect(mmwp.update_mmw)
    mmw_thread.start()
  except ConnectionRefusedError:
    log.error("connection refused to mmw: disabling")

  try:
    cam_sock = socket.create_connection((host, config.cam_server.port))
    cam_thread = CamThread(cam_sock)
    cam_thread.new_data.connect(mmwp.update_cam)
    cam_thread.start()
  except ConnectionRefusedError:
    log.error("connection refused to camera: disabling")

  pg.exec()

if __name__ == '__main__':
  main()
