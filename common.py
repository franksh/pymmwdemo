#!/usr/bin/env python3
import logging
import os
logging.basicConfig(
  format='%(asctime)s :: %(levelname)s %(name)s :: %(filename)s:%(funcName)s :: %(message)s',
  level=os.environ.get('LOGLEVEL', 'INFO').upper())
log = logging.getLogger('common')

from pathlib import Path
import ast
import pprint
import socketserver as ss
import sys
import time


DEFAULT_CONFIG_FILE = 'pysensor.ini'
DEFAULT_CONFIG_TEXT = '''#
# This file is interpreted as a single Python literal.
#
# Normal Python syntax applies, but no code can be executed.
#
# It should evaluate to a dictionary.

{
  'mmw_server': {
    'pid_file': 'mmw_server.pid',
    'enabled': True,
    'addr': '0.0.0.0',
    'port': 2150,

    'setup_file': 'config_ex.cfg',
    'client_port': ('/dev/ttyACM0', 115200),
    'data_port':   ('/dev/ttyACM1', 921600),
    'data_timeout': 0.5,
  },
  'cam_server': {
    'pid_file': 'cam_server.pid',
    'enabled': True,
    'addr': '0.0.0.0',
    'port': 2151,

    'capture_resolution': (2592, 1944), # (3280, 2464),
    # Output resolution should be a multiple of 32?
    'output_resolution': (512, 384),
    'format': 'rgb',
    'framerate': 20,
  },
}

'''


class jsdict(dict):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.__dict__ = self

  @classmethod
  def from_dict(self, obj):
    """Recursively convert `dict` object to `jsdict`.

    """
    return jsdict((k,jsdict.from_dict(v)) if isinstance(v, dict) else (k,v) for k,v in obj.items())


def load_config(filename=DEFAULT_CONFIG_FILE, defaults=DEFAULT_CONFIG_TEXT):
  """Simple config load/creation using Python's `literal_eval()`. Simpler and in
  some cases better than `configparser`.

  """
  fname = Path(filename)
  if not fname.exists():
    fname.write_text(DEFAULT_CONFIG_TEXT)

  try:
    return jsdict.from_dict(ast.literal_eval(Path(filename).read_text()))
  except Exception:
    log.exception("failed to load config file %s", filename)
    return None


def kill_pid(pid):
  log.warning("attempting to kill PID %d", pid)
  r = os.system('kill -9 "%d"' % pid)
  if r != 0:
    log.error("failed to kill PID %d!", pid)
  # Some arbitrary delay for safety?
  time.sleep(0.2)


class SensorServer(ss.ForkingTCPServer):
  def __init__(self, opts, handler, *args, **kwargs):
    self._opts = opts
    self.allow_reuse_address = True
    super().__init__((opts.addr, opts.port),
                     lambda *args, **kwargs: handler(opts, *args, **kwargs),
                     *args,
                     **kwargs)
    log.info("server started at %s : %s", opts.addr, opts.port)
    log.info("server options: %s", opts)

  def verify_request(self, req, client):
    pidf = Path(self._opts.pid_file)
    if pidf.exists():
      kill_pid(int(pidf.read_text()))
      pidf.unlink()
    return True

  def handle_error(self, req, client):
    log.exception("exception in client: %s", client)


class ClientHandler(ss.BaseRequestHandler):
  def __init__(self, opts, *args, **kwargs):
    self._opts = opts
    super().__init__(*args, **kwargs)

  def setup(self):
    pid = os.getpid()
    log.info("client init: %s (PID %d)", self.client_address, pid)
    Path(self._opts.pid_file).write_text(str(os.getpid()))

  def finish(self):
    Path(self._opts.pid_file).unlink(missing_ok=True)
    log.info("client finished: %s", self.client_address)
